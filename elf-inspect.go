package main

import (
	"bytes"
	"debug/elf"
	"fmt"
	"log"
	"strconv"

	"os"

	"github.com/knightsc/gapstone"
)

func compareSections(x *elf.Section, y *elf.Section) (int, error) {
	xb, err := x.Data()
	if err != nil {
		return 0, err
	}
	yb, err := y.Data()
	if err != nil {
		return 0, err
	}

	return bytes.Compare(xb, yb), nil
}

func disassembleBuffer(code []byte) {
	engine, err := gapstone.New(
		gapstone.CS_ARCH_X86,
		gapstone.CS_MODE_64,
	)

	insns, err := engine.Disasm(
		code, // code buffer
		0,    // starting address
		0,    // insns to disassemble, 0 for all
	)

	if err == nil {
		log.Printf("Disasm:\n")
		for _, insn := range insns {
			log.Printf("0x%x:\t%s\t\t%s\n", insn.Address, insn.Mnemonic, insn.OpStr)
		}
		return
	}
}

func main() {
	GCC, err := elf.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	GCCDDC, err := elf.Open(os.Args[2])
	if err != nil {
		log.Fatal(err)
	}

	for _, section := range GCC.Sections {
		found := false
		for _, ddcSection := range GCCDDC.Sections {
			if section.Name == ddcSection.Name {
				found = true
				diff := false
				differ, err := compareSections(section, ddcSection)
				fmt.Println(section.Name)
				if err != nil {
					fmt.Println("Failed to compare sections")
					diff = true
				} else {
					if differ != 0 {
						diff = true
					} else {
						fmt.Println("Matches")
					}
				}

				if section.Name == ".fini" {
					code, _ := section.Data()
					disassembleBuffer(code)
				}

				if diff {
					fmt.Printf("Differs at: %v\n", differ)
					fmt.Println(section.Offset)
					fmt.Println(ddcSection.Offset)
					fmt.Printf("Native offset:   0x%v  Native size: 0x%v\n", strconv.FormatUint(section.Offset, 16), strconv.FormatUint(section.Size, 16))
					fmt.Printf("DDC offset   :   0x%v  DDC size:    0x%v\n", strconv.FormatUint(ddcSection.Offset, 16), strconv.FormatUint(ddcSection.Size, 16))
				}

				fmt.Printf("-----------------------------\n")
			}
		}

		if !found {
			fmt.Println(section.Name + " doesn't appear in the DDC binary")
		}
	}

	fmt.Println(err)
}
